from datetime import datetime
import pandas as pd
import numpy as np
from typing import List, Dict
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import matplotlib.ticker as mticker
import cartopy.feature as cf
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from plot_alpha_beta_diversity import main_diversity
# create dict of sampledates and their lag information

df_ = pd.read_csv("data/mosaic/baceuk/MetaData_MOSAiC.csv", sep=";")
df_lag = df_[["Sampling date","lag"]]
lag_dict = df_lag.groupby("Sampling date").agg("first").to_dict()["lag"]

# color for the lags
lag_color_dict = {"1" : "blue", "2" : "green", "3" : "red", "4" : "orange", "5" : "yellow"}

def find_closest_date(target_date:str , date_list: List[str]):
    """
    Find the closest date
    :param target_date: date to look for
    :param date_list: list of all possible dates
    :return: date closest to target_date
    """
    # Convert target date to datetime object
    target_datetime = datetime.strptime(target_date, '%Y-%m-%d')

    # Convert all dates in the list to datetime objects
    date_objects = [datetime.strptime(date, '%Y-%m-%d') for date in date_list]

    # Find the closest date in the list
    closest_date = min(date_objects, key=lambda x: abs(x - target_datetime))

    # Convert closest date back to string format
    closest_date_str = closest_date.strftime('%Y-%m-%d')

    return closest_date_str


def create_dict(dic: Dict, val:str):
    """
    Create a dictionary request with fall back to next date
    :param dic: Dict
    :param val: str
    :return: str
    """
    try:
        return dic[val]
    except:
        return dic[find_closest_date(val, dic.keys())]

def katja_data_descriptor_process(abu_path: str, save_path:str):
    """
    Add lag information and color to dataframe and save it as csv
    :param abu_path: path to the abundance data
    :param save_path: path to save the csv file
    :return:
    """
    df_abu = pd.read_csv(abu_path,sep=";")
    df_abu.set_index("ASV",inplace=True)
    df_abu = df_abu.T
    df_abu["lag"] = df_abu.index
    df_abu["lag"] = df_abu["lag"].apply(lambda x: create_dict(lag_dict,x[:10].replace("_","-")))
    df_abu["lag_color"] = df_abu["lag"].apply(lambda x: lag_color_dict[str(x)])
    df_abu["depth"] = df_abu.index
    df_abu["depth"] = df_abu["depth"].apply(lambda x: x.split("_")[-1])
    # agg the data
    df_abu_grouped = pd.DataFrame(df_abu.groupby(["depth","lag","lag_color"]).agg(sum))
    df_abu_grouped.reset_index(inplace=True)
    df_abu_grouped["name"]= df_abu_grouped["depth"] +"-lag-"+ df_abu_grouped["lag"].astype(str)
    df_abu_grouped.drop(["depth","lag"], axis=1, inplace=True)
    df_abu_grouped.set_index("name",inplace=True)
    df_abu_grouped.to_csv(save_path, sep=";")

def enrich_taxa_table_with_lag_infos_from_abu(abu_path :str, taxa_path :str, save_path:str):
    """
    Enrich the taxa table with lag infos

    :param abu_path: path to the abundance table
    :param taxa_path: path to the taxa table
    :param save_path: path for the output
    :return: None
    """
    df_ab = pd.read_csv(abu_path,sep=";")
    water_cols = ["ASV"]+[col for col in df_ab.columns if "Water" in col]
    df_ab = df_ab[water_cols]
    df_ab.set_index("ASV", inplace=True)
    df_t_t = df_ab.T
    df_t_t["lag"]=df_t_t.index
    df_t_t["lag"] = df_t_t["lag"].apply(lambda x:x[:10].replace("_","-"))
    df_t_t["lag"] =df_t_t["lag"].apply(lambda x: "Lag"+str(create_dict(lag_dict, x)))
    print(df_t_t["lag"].value_counts())
    df_t_t.groupby("lag").agg(np.sum).T.applymap(lambda x:1 if x > 0 else 0)
    df_lag_abu =df_t_t.groupby("lag").agg(np.sum).T.applymap(lambda x:1 if x > 0 else 0)
    df_euk_taxa = pd.read_csv(taxa_path,sep=";")
    df_taxa = df_euk_taxa
    df_taxa.set_index("ASV",inplace=True)
    # join the lag info
    df_taxa_div = df_taxa.join(df_lag_abu, how="inner")
    df_taxa_div.to_csv(save_path, sep=";")


def plot_arctic_map_with_sample_point_colored_by_lags(df, save_path):
    """
    Plot Arctic Map and add sample points colored by lag from df
    :param df: pandas dataframe with long, lat and lag column
    :return:
    """
    # Define Arctic bounds for zooming in
    xmin = -130
    xmax = 130
    ymin = 79
    ymax = 89
    # Define projection
    proj = ccrs.NorthPolarStereo(central_longitude=(xmin + xmax) / 2)

    # Create figure and axes
    fig = plt.figure(figsize=(12, 8))
    ax = fig.add_subplot(1, 1, 1, projection=proj)

    # Plot ocean borders
    ocean = cf.NaturalEarthFeature('physical', 'ocean', scale='50m', edgecolor='k', facecolor='lightblue', lw=1,
                                   linestyle='-')
    ax.add_feature(ocean)

    # Colored land background
    land = cf.NaturalEarthFeature('physical', 'land', scale='50m', facecolor='snow', lw=1, linestyle='--')
    ax.add_feature(land)

    # Set extent to Arctic region
    ax.set_extent([xmin, xmax, ymin, ymax], crs=ccrs.PlateCarree())

    # Set gridlines
    gl = ax.gridlines(draw_labels=True, crs=ccrs.PlateCarree(), x_inline=False, y_inline=False)
    gl.xlocator = mticker.FixedLocator(np.arange(-180, 181, 10))
    gl.ylocator = mticker.FixedLocator(np.arange(50, 91, 5))
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER

    # Plot coordinates
    colors = {1: 'blue', 2: 'green', 3: 'red', 4: 'orange', 5: 'yellow'}
    list_of_labels = []
    for ind, row in df.iterrows():
        ax.plot(row["Long"], row["Lat"], marker='o', color=colors[row["lag"]], markersize=4,
                transform=ccrs.PlateCarree(),
                label="Lag " + str(row["lag"]) if row["lag"] not in list_of_labels else "")
        list_of_labels.append(row["lag"])
        list_of_labels = list(set(list_of_labels))

    # Add legend
    plt.legend(loc='lower right')
    plt.savefig(save_path, dpi=300)  # Adjust dpi as needed
    plt.show()


if __name__ == "__main__":
    # plot the map
    plot_arctic_map_with_sample_point_colored_by_lags(df_,save_path ='data/mosaic/baceuk/figures/baceuk_arctic_plot_zoom.jpg')

    # input data for rarecurves BAC
    katja_data_descriptor_process("data/mosaic/baceuk/BacEuk/ProkRawAbundMOSAiC.csv", "data/mosaic/baceuk/BAC_water_sample_compri.csv")
    # input data for rarecurves EUK
    katja_data_descriptor_process("data/mosaic/baceuk/BacEuk/EukRawAbundMOSAiC.csv", "data/mosaic/baceuk/EUK_water_sample_compri.csv")

    # input for bio diversity BAC
    enrich_taxa_table_with_lag_infos_from_abu('data/mosaic/baceuk/BacEuk/ProkNormAbundMOSAiC.csv', 'data/mosaic/baceuk/BacEuk/ProkTaxaMOSAiC.csv', "data/mosaic/baceuk/lag_table_water_bacs.csv")
    # input for bio diversity EUK
    enrich_taxa_table_with_lag_infos_from_abu('data/mosaic/baceuk/BacEuk/EukNormAbundMOSAiC.csv', 'data/mosaic/baceuk/BacEuk/EukTaxaMOSAiC.csv', "data/mosaic/baceuk/lag_table_water_euks.csv")

    # euk Diversity
    main_diversity(path_figures="data/mosaic/baceuk/figures/euk", taxa_level="Supergroup",
                   input_path="data/mosaic/baceuk/lag_table_water_euks.csv", mod="Water")

    # Bac Diversity
    main_diversity(path_figures="data/mosaic/baceuk/figures/bac", taxa_level="Phylum",
                   input_path="data/mosaic/baceuk/lag_table_water_bacs.csv", mod="Water")