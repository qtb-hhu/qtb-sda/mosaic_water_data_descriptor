import skbio as sb
from matplotlib.colors import ListedColormap
from matplotlib import cm
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

SMALL_SIZE = 20
MEDIUM_SIZE = 20
BIGGER_SIZE = 20
MEDIUM = 25
TICK = 20
I_SIZE = 8
plt.rc('font', size=SMALL_SIZE)  # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM)  # fontsize of the x and y labels
plt.rc('xtick', labelsize=TICK)  # fontsize of the tick labels
plt.rc('ytick', labelsize=TICK)  # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM)  # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)
linewidth = 4

def isna_col(col):
    """
    Checks if the given column is a nan
    :param col:
    :return:
    """
    if np.isnan(col):
        return 0
    else:
        return 1

def bins(col):
    """
    Bins column
    :param col:
    :return:
    """
    if col == 0:
        return 0
    elif col <= 10:
        return 1
    elif col <= 20:
        return 2
    elif col <= 30:
        return 3
    elif col <= 40:
        return 4
    elif col <= 50:
        return 5
    elif col <= 100:
        return 6
    elif col <= 150:
        return 7
    else:
        return 8


def log_if_not_null(col):
    """
    solve log from 0
    :param col:
    :return:
    """
    if col != 0.0:
        return np.log(col)
    else:
        return 0

def plot_richness(alpha_diversity_data, save_path_temp, sample_colors=None):
    """
    Plot richness
    :param alpha_diversity_data:
    :param save_path_temp:
    :param sample_colors:
    :return:
    """
    # Set seaborn style
    sns.set(style="whitegrid")

    # Number of subplots
    num_metrics = alpha_diversity_data.shape[1]

    # Calculate the number of rows needed
    num_rows = (num_metrics + 1) // 2

    # Create subplots with 2 columns
    fig, axes = plt.subplots(nrows=num_rows, ncols=2, figsize=(14, 4 * num_rows))

    # Flatten the 2D array of subplots
    axes = axes.flatten()

    # Plot each metric on a separate subplot with specified sample colors
    for i, metric in enumerate(alpha_diversity_data.columns):
        alpha_diversity_data[metric].plot(kind='bar', ax=axes[i], rot=0, color=sample_colors, edgecolor='black')
        axes[i].set_ylabel(f'{metric}', fontsize=20)
        axes[i].set_xlabel('Cluster', fontsize=20)
        # axes[i].set_title(f'Alpha Diversity - {metric}', fontsize=14)
        axes[i].tick_params(axis='both', which='both', length=0, labelsize=16)  # Hide ticks

    # Adjust layout for better spacing
    plt.tight_layout()

    # Show the plot


    plt.savefig(save_path_temp, dpi=200, bbox_inches='tight')
    plt.show()

# Function to calculate beta
def calculate_beta(x, y,num_cl):
    """
    Calculate the beta diversity
    :param x:
    :param y:
    :param num_cl:
    :return:
    """
    coun = np.zeros((2, len(x.values)))
    coun[0, :] = x.values.ravel()
    coun[1, :] = y.values.ravel()
    a = sb.diversity.beta_diversity(metric="braycurtis", counts=coun)
    beta = np.round(a[0][1], num_cl)
    return beta

 # Generate the list of arrays and their names
def generate_temp_list_and_names(clu_list_, temp_list):
    """
    Generate a list of temps lists
    :param clu_list_:
    :param temp_list:
    :return:
    """
    temp_list = temp_list  # Modify size or number of arrays as needed
    clu_list_ = clu_list_  # Names for the arrays
    return temp_list, clu_list_

def main_diversity(path_figures, taxa_level, input_path, mod):
    """
    Main function
    :return:
    """
    df_f4_count = pd.read_csv(input_path, sep=";")
    df_f4_count.rename(columns={"ASV": "Nodes"}, inplace=True)
    bins_list = ["0", "(0,10]", "(10,20]", "(20,30]", "(30,40]", "(40,50]", "(50,100]", "(100,150]", "(150,400]"]
    df_f4_count["count"] = 1

    temp_list = []
    temp_list_ = []
    clu_list_ = []
    no_asv = []

    for lag in sorted(["Lag1", "Lag2", "Lag3", "Lag4", "Lag5"]):
        clu_list_.append(lag)
        df_temp = df_f4_count[df_f4_count[lag] == 1]
        # df_temp = df_temp.reindex(df_f4_count, fill_value=0)
        no_asv.append(df_temp.shape[0])
        f4 = df_temp.groupby(taxa_level).agg({'count': ['sum']}).sort_values(by=[('count', 'sum')])

        f4["count_n"] = f4[('count', 'sum')] / np.sum(f4[('count', 'sum')])
        f4_count = f4.values.reshape(1, -1).ravel().tolist()
        f4["log"] = f4["count_n"].apply(log_if_not_null)
        f4["sh"] = f4["log"] * f4["count_n"]
        f4["bins"] = f4[('count', 'sum')].apply(bins)
        # f4 = f4.sort_values('Class')
        F = -np.sum(f4)
        temp_list_.append(f4_count)
        temp_list.append(f4)

    f4 = df_f4_count.groupby(taxa_level).agg({'count': ['sum']}).sort_values(by=[('count', 'sum')])
    f4__ = f4
    f4["count_n"] = f4[('count', 'sum')] / np.sum(f4[('count', 'sum')])
    f4_count = f4.values.reshape(1, -1).ravel().tolist()
    f4["log"] = f4["count_n"].apply(log_if_not_null)
    f4["sh"] = f4["log"] * f4["count_n"]
    f4["bins"] = f4[('count', 'sum')].apply(bins)


    viridis = cm.get_cmap('hsv', len(df_f4_count[taxa_level].drop_duplicates()))
    newcolors = viridis(np.linspace(0, 1, len(df_f4_count[taxa_level].drop_duplicates())))
    newcolors2 = newcolors.copy()
    newcmp = ListedColormap(newcolors2)
    color_dict = {}
    counter = 0
    for ent in df_f4_count[taxa_level].drop_duplicates():
        color_dict[ent] = newcolors2[counter, :]
        counter += 1

    coun = np.zeros((2, len(f4_count)))
    coun[0, :] = np.array(f4_count)
    coun[1, :] = np.array(f4_count)
    a = sb.diversity.beta_diversity(metric="hamming", counts=coun)
    beta = np.round(a[0][1], 10)

    # Assuming 'color_dict' and 'bins_list' are predefined in your code

    fig, axs = plt.subplots(3, 2, figsize=(30, 22), sharey=True)  # Create a 5x2 grid for subplots with shared y-axis
    chao = []
    shan = []
    simp = []
    noasv = []

    for ind, temp in enumerate(temp_list):
        f4_count = temp["count_n"]
        f4 = temp_list[ind]
        row = ind // 2  # Calculate row for the subplot
        col = ind % 2  # Calculate column for the subplot

        ax = axs[row, col]  # Select the current subplot

        # Use Seaborn to set aesthetic parameters
        sns.set(style="whitegrid", font_scale=1.2)

        for data in f4.iterrows():
            w = data[1]["bins"]
            # w = w.reindex(f4__.index, fill_value=0)
            ax.bar(data[0], w, color=color_dict[data[0]])
            ax.set_yticklabels(bins_list)
            ax.tick_params(axis='x', rotation=90)  # Rotate x-axis labels

        ax.set_xlabel(taxa_level, fontsize=15)
        ax.set_ylabel("Number of ASVs", fontsize=15)
        ax.set_title(f"{clu_list_[ind]}")
        # Adjust positioning of text annotations
        z = f4["count_n"]
        z = z.reindex(f4__.index, fill_value=0)

        chao.append(np.round(sb.diversity.alpha.chao1(counts=z), 2))
        shan.append(np.round(sb.diversity.alpha.shannon(counts=z, base=np.e), 2))
        simp.append(np.round(sb.diversity.alpha.simpson(counts=z), 2))
        noasv.append(np.round(no_asv[ind], 2))

        plt.tight_layout()  # Adjust layout for better appearance
        axs[-1, -1].axis('off')
    # Show and save the plot
    plt.show()
    save_path_temp = f'{path_figures}/Example_KDD_Sup_Figure_1_alpha__div_F4_each_cluster_{mod}_{taxa_level}.png'
    fig.savefig(save_path_temp, dpi=200, bbox_inches='tight')

    df_alpha = pd.DataFrame()
    df_alpha["Cluster"] = clu_list_
    df_alpha["Chao1 - Richness"] = chao
    df_alpha["Shannon Entropy"] = shan
    df_alpha["Simpson-Index"] = simp
    df_alpha["No. ASVs"] = noasv
    # df_alpha["Cluster"] = df_alpha["Cluster"].apply(lambda x: name_dict_new[str(x)])
    clu_list_ = df_alpha["Cluster"].to_list()
    df_alpha.set_index("Cluster", inplace=True)

    num_cl = 5

    # Generate the list of arrays and their names
    temp_list, clu_list_ = generate_temp_list_and_names(clu_list_, temp_list)

    # Calculate beta values for each pair in the list
    beta_matrix = np.zeros((num_cl, num_cl))
    for i in range(num_cl):
        for j in range(num_cl):
            x = temp_list[i]["count"]
            y = temp_list[j]["count"]
            x = x.reindex(f4__.index, fill_value=0)
            y = y.reindex(f4__.index, fill_value=0)
            print(x.shape)
            print(y.shape)
            beta_matrix[i, j] = calculate_beta(x, y, num_cl)

    # Plot the matrix as a heatmap
    # Plot the matrix as a heatmap
    plt.figure(figsize=(8, 6))
    sns.heatmap(beta_matrix, annot=True, cmap='coolwarm', fmt='.1f', cbar=True, square=True, linewidths=1)

    sns.set_style('white')
    # Logarithmic normalization

    # Annotate the heatmap with cluster names
    plt.xticks(np.arange(num_cl) + 0.5, clu_list_, rotation=90)
    plt.yticks(np.arange(num_cl) + 0.5, clu_list_, rotation=0)

    # plt.title('Beta Diversity Matrix', fontsize=16)
    plt.xlabel('Cluster', fontsize=12)
    plt.ylabel('Cluster', fontsize=12)

    plt.tight_layout()  # Adjust layout for better appearance

    save_path_temp = f'{path_figures}/Example_KDD_Beta_diversity_heatmap_braycurtis_{mod}_{taxa_level}.png'
    plt.savefig(save_path_temp, dpi=200, bbox_inches='tight')

    plt.show()

    x = x.reindex(f4__.index, fill_value=0)

    sample_colors = ['blue', 'green', 'red', 'orange', 'yellow']

    plot_richness(df_alpha,  save_path_temp = f'{path_figures}/Example_KDD_Alpha_diversity_barplot_{mod}_{taxa_level}.png', sample_colors=sample_colors)

if __name__ == '__main__':
    main_diversity(path_figures="data/mosaic/baceuk/figures", taxa_level="Supergroup", input_path="data/mosaic/baceuk/lag_table_water_euks.csv", mod="Water")



